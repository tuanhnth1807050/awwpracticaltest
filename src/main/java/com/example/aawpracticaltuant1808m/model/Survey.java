package com.example.aawpracticaltuant1808m.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

public class Survey {
    @NotNull(message = "Age is required")
    @Min(value = 1, message = "Age must be higher than 0")
    @Max(value = 100, message = "Age must be lower than 100")
    public int age;

    @NotNull(message = "Product is required")
    public List<String> product;

    @NotNull(message = "Country is required")
    public String country;

    @NotNull(message = "Gender is required")
    public String gender;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<String> getProduct() {
        return product;
    }

    public void setProduct(List<String> product) {
        this.product = product;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
