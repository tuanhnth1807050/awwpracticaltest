package com.example.aawpracticaltuant1808m;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AawPracticaltuanT1808mApplication {

    public static void main(String[] args) {
        SpringApplication.run(AawPracticaltuanT1808mApplication.class, args);
    }

}
